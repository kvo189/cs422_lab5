//
//  EditAlertViewController.swift
//  CS422L
//
//  Created by Jonathan Sligh on 2/18/21.
//

import UIKit

class EditAlertViewController: UIViewController {

    @IBOutlet var alertView: UIView!
    @IBOutlet var termEditText: UITextField!
    @IBOutlet var definitionEditText: UITextField!
    //card from FlashCardSetDetailViewController
    var card: Flashcard = Flashcard()
    var cardIndex: Int = 0
    //use this later to do things to the flashcards potentially
    var parentVC: FlashCardSetDetailViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setup()
    }
    
    func setup()
    {
        alertView.layer.cornerRadius = 8.0
        //set term/def
        termEditText.text = card.term
        definitionEditText.text = card.definition
        //make it so it shows this is editable
        termEditText.becomeFirstResponder()
    }
    
    @IBAction func deleteFlashcard(_ sender: Any) {
        //nothing yet but eventually delete the flashcard
        
        self.dismiss(animated: false, completion:{
            self.parentVC?.deleteCard(index: self.cardIndex)
        })
    }
    
    // 3. Save Flashcard to edited values
    @IBAction func doneEditing(_ sender: Any) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        parentVC?.cards[cardIndex].term = termEditText.text
        parentVC?.cards[cardIndex].definition = definitionEditText.text
        
        do {
            try managedContext.save()
            parentVC?.tableView.reloadRows(at: [IndexPath(row: cardIndex, section: 0)], with: .automatic)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
        
        self.dismiss(animated: false, completion: nil)
    }
    
}
